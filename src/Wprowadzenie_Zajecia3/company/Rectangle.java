package Wprowadzenie_Zajecia3.company;


public class Rectangle {

    // - to private; + to public

    private double sideA;
    private double sideB;
    private String color;

    public Rectangle() {
        sideA = 10;
        sideB = 10;
        color = "czerwony";
    }

    public Rectangle(double sideA, double sideB, String color) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.color = color;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public String getColor() {
        return color;
    }

    public double getCalcArea(){
        return sideA * sideB;
    }

    public double getCalcPerimeter(){
        return 2 * (sideA + sideB);
    }


}
