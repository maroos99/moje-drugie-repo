package Wprowadzenie_Zajecia3.company;

public class Main {

    public static void main(String[] args) {
        //ćwiczenie 1:

//        Piłkarz p = new Piłkarz("Lewandowski", 33, "Bayern Monachium", 9, 1000000);
//        p.introduceYourself();



        //ćwiczenie 2:
        Rectangle r1 = new Rectangle(2.0, 3.0, "niebieski");
        Rectangle r2 = new Rectangle(4.0, 2.0, "zielony");
        Rectangle r3 = new Rectangle(1.0, 3.0, "czarny");
        Rectangle r4 = new Rectangle();

        System.out.println("Informacje o prostokacie 1: ");
        printInformation(r1);

        System.out.println("");
        System.out.println("Informacje o prostokacie 2: ");
        printInformation(r2);

        System.out.println("");
        System.out.println("Informacje o prostokacie 3:");
        printInformation(r3);

        System.out.println("");
        System.out.println("Informacje o prostokacie 4 z wartosciami domyslnymi:");
        printInformation(r4);

    }

    public static void printInformation(Rectangle r){
        System.out.println("Bok A " + r.getSideA() + " Bok B " + r.getSideB() + " kolor prostokąta: " + r.getColor());
        System.out.println("Pole prostokata: " + r.getCalcArea() + " Obwod prostokata: " + r.getCalcPerimeter());
    }
}
