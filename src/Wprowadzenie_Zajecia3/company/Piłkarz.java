package Wprowadzenie_Zajecia3.company;

public class Piłkarz {

    //mozna zainicjalizowac statycznie wartosci, wtedy wyswietla sie one, gdy utworzymy nowy konstruktor i nie damy mu wszystkich pol
    String surname;
    int age;
    String team;
    int number;
    int salary;

    public Piłkarz(String surname, int age, String team, int number, int salary) {
        this.surname = surname;
        this.age = age;
        this.team = team;
        this.number = number;
        this.salary = salary;
    }

    public void introduceYourself(){
        System.out.println("Czesc! Nazywam sie " + surname);
        System.out.println("Mam " + age + " lat");
        System.out.println("Gram w druzynie " + team + " z numerem " + number);
        System.out.println("Moja pensja wynosi: " + salary);
    }

}
